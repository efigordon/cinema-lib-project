import { Directive } from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl } from '@angular/forms';
import { MovieService } from "./movie.service";

@Directive({
  selector: '[validateUniqueNameEdit]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CustomEditValidator,
      multi: true
    }
  ]
})
export class CustomEditValidator implements Validator {
  listOfMovies = [];
  validAns: boolean;
  initialValue;
  constructor(private movieService: MovieService) {

  }
  ngOnInit() {
  

  }

  validate(control: AbstractControl): { [key: string]: any } | null {
    if(this.initialValue==null) {
      this.initialValue = control.value;
    }

    this.listOfMovies = this.movieService.getMovies();
    this.validAns = true;
    for (let movie of this.listOfMovies) {
      if (movie['Title'] == control.value && (this.initialValue!=null && this.initialValue!=control.value)) {
        this.validAns = false;
      }
    }
    return this.validAns ? null : { 'defaultSelected': true };

  }
}
