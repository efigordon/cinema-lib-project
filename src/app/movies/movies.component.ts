import { Component, OnInit } from '@angular/core';
import { MovieService } from "../movie.service";
import { MatDialog } from '@angular/material';
import { DeleteMovieComponent } from "./delete-movie.component";
import { EditMovieComponent } from "../edit-movie/edit-movie.component";
import { NgForm } from '@angular/forms';
import { AddMovieComponent } from "../add-movie/add-movie.component";
@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  result: NgForm;
  listOfMovies = [];
  alertShow = true;
  public msg = "";
  newMovie: { "Title": string; "Year": string; "Runtime": string; "Genre": string; "Plot": string; "Director": string; "Poster": string; };
  constructor(private movieService: MovieService, private dialog: MatDialog) { }


  ngOnInit() {
    this.listOfMovies = [];
    this.listOfMovies = this.movieService.getMovies();
  }
  addMessage(msg) {
    this.msg = msg;
    this.alertShow = false;

    setTimeout(() => {
      this.msg = "";
      this.alertShow = true;
    }, 2200);


  }

  editMovie(movie) {

    let editRef = this.dialog.open(EditMovieComponent, {
      height: '600px',
      width: '800px',
      data: {
        movie: movie
      }
    });

    editRef.afterClosed().subscribe(
      result => {

        if (!result) {
        }
        else {
          movie['Title'] = result.value.movieTitle;
          movie['Year'] = result.value.movieYear;
          movie['Director'] = result.value.movieDirector;
          movie['Genre'] = result.value.movieGenre;
          movie['Plot'] = result.value.movieDescription;
          movie['Runtime'] = result.value.movieRuntime;
          
          this.addMessage(movie['Title'] + " Was Edited Succesfuly.");
        }

      });
     

  }

  deleteMovie(movie) {
    const dialogRef = this.dialog.open(DeleteMovieComponent, {

      data: {
        title: movie.Title
      }
    });
    dialogRef.afterClosed().subscribe(
      result => {
        if (result) {
          this.listOfMovies = this.listOfMovies.filter(item => item != movie);
          this.movieService.setMovies(this.listOfMovies);
        }
        else {

        }
      }
    );

  }
  addNewMovie() {
    const ref = this.dialog.open(AddMovieComponent, {
      height: '600px',
      width: '800px',
      data: {
      }
    });

    ref.afterClosed().subscribe(
      result => {
        if (!result) {
        }
        else {
          this.newMovie = {
            "Title": result.value.movieTitle,
            "Year": result.value.movieYear,
            "Runtime": result.value.movieRuntime,
            "Genre": result.value.movieGenre,
            "Plot": result.value.movieDescription || "Not fiiled yet.",
            "Director": result.value.movieDirector,
            "Poster": "assets/logo.png"
          };
          this.listOfMovies = [this.newMovie, ...this.listOfMovies];
          this.movieService.setMovies(this.listOfMovies);
          this.addMessage(this.newMovie['Title'] + " Was Added Succesfuly.");
        }

      });
  }


}
